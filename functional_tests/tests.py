from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_in_li(self,row_text):
        table = self.browser.find_element_by_id('idResult')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text,[row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)

        self.assertIn('Currency Exchange', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Currency Exchange', header_text)

        amtbox = self.browser.find_element_by_id('amt')
        amtbox.send_keys('0')
        Cfrombox = self.browser.find_element_by_id('Cfrom')
        Cfrombox.send_keys('USD')
        Ctobox = self.browser.find_element_by_id('Cto')
        Ctobox.send_keys('THB')

        self.assertEqual(amtbox.get_attribute('value'),'10')
        amtbox.send_keys(Keys.ENTER)

        self.check_in_li('355.0')
        self.fail('Finish the test!')
    
