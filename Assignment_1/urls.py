from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'Currency_Exchange.views.home_page', name='home'),
    url(r'^Currency_Exchange/TestPath/$',
        'Currency_Exchange.views.view_list',
        name='view_list'
    ),
    url(r'^Currency_Exchange/Rates/$',
        'Currency_Exchange.views.get_rates',
        name='get_rates'
    ),
    url(r'^json_load_data/$',
        'Currency_Exchange.views.json_load_data2',
        name='json_load_data2'
    ),

    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),

    #url(r'^testDB/', 'Currency_Exchange.views.testDB' ,
    #    name='testDB'
    #),
)
