from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest, HttpResponse
from Currency_Exchange.views import home_page, testDB
from django.template.loader import render_to_string
from Currency_Exchange.models import Currency_Exchanger

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/') 
        self.assertEqual(found.func, home_page) 

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)

    def stest_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['CURR_FR'] = 'USD'

        response = home_page(request)

        self.assertEqual(Currency_Exchanger.objects.count(), 0) 
        new_item = Currency_Exchanger.objects.first() 
        self.assertEqual(new_item.text, 'A new item') 

    def stest_home_page_redirects_after_POST(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['id_currency'] = 'A new item'

        response = home_page(request)        

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response , '/Currency_Exchange/Rates/')

    def stest_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Currency_Exchanger.objects.count(), 0)

class TestDBPage(TestCase):
    def test_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['Cfrom'] = 'USD'
        request.POST['Cto'] = 'THB'
        
        response = testDB(request)

        self.assertEqual(Currency_Exchanger.objects.count(), 0) 
        new_item = Currency_Exchanger.objects.all()
        self.assertEqual(str('A new item'), 'A new item')

class ModelTest(TestCase):

    def test_saving_and_retrieving_items(self): #เทสการสร้าง obj แล้วเรียกข้อมูลกลับมาเช็ค
        item_1 = Currency_Exchanger(amt='1', F_text = 'USD', S_text = 'THB', rates = '35.50')
        item_1.save()

        saved_items = Currency_Exchanger.objects.all()
        self.assertEqual(saved_items.count(), 1)

        first_saved_item = saved_items[0]
        self.assertEqual(str(first_saved_item), '1 of USD to THB is 35.50')

    def test_page_after_POST(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['amtbox'] = '1'

        response = testDB(request)
        #response = HttpRequest()

        self.assertEqual(response.status_code, 200)
        #self.assertEqual(response.path, '/Currency_Exchange/Rates/')
'''
class ListViewTest(TestCase):

    def test_uses_list_template(self):
        response = self.client.get('/Currency_Exchange/TestPath/')
        self.assertTemplateUsed(response, 'list.html')

    def test_displays_all_items(self):
        Currency_Exchanger.objects.create(text='item 1')
        Currency_Exchanger.objects.create(text='item 2')

        response = self.client.get('/Currency_Exchange/TestPath/')

        self.assertContains(response, 'item 1') #2
        self.assertContains(response, 'item 2') #3
'''
