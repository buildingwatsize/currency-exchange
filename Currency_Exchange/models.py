from django.db import models

class Currency_Exchanger (models.Model):    
    amt = models.CharField(default='1',max_length=10) #AMOUNT COST
    F_text = models.TextField(default='USD',max_length=3) #first text input
    S_text = models.TextField(default='THB',max_length=3) #second text input
    rates = models.TextField(default='') #RATEs
    result = models.TextField(default="1") #RESULT

    def __str__(self):              # __unicode__ on Python 2
        return self.amt+' of '+self.F_text+' to '+ self.S_text+' is '+self.result
