# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Currency_Exchanger',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('amt', models.CharField(max_length=10, default='1')),
                ('F_text', models.CharField(max_length=3, default='USD')),
                ('S_text', models.CharField(max_length=3, default='THB')),
                ('rates', models.TextField(default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
