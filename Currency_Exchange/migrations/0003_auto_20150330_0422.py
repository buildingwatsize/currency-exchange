# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Currency_Exchange', '0002_currency_exchanger_result'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currency_exchanger',
            name='F_text',
            field=models.TextField(max_length=3, default='USD'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='currency_exchanger',
            name='S_text',
            field=models.TextField(max_length=3, default='THB'),
            preserve_default=True,
        ),
    ]
