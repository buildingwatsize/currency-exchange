# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Currency_Exchange', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='currency_exchanger',
            name='result',
            field=models.TextField(default='1'),
            preserve_default=True,
        ),
    ]
