from django.shortcuts import redirect, render, get_object_or_404
from Currency_Exchange.models import Currency_Exchanger
from django.http import StreamingHttpResponse

#def home_page(request):
    #if request.method == 'POST':
        #Item.objects.create(text=request.POST['item_text'])
        #return render(request, 'home.html')
    #return render(request, 'home.html')

def view_list(request):
    item_all = Currency_Exchanger.objects.all()
    return render(request, 'list.html', {'item_all': item_all})

def get_rates(request):
    return render(request, 'rates.html')

def json_load_data2(request):
    return render(request, 'jsonload.html')

def testDB(request):
    return "";
def home_page(request):

    if request.method == 'POST':
        amt = float(request.POST.get('amt',"1"))
        Cfrom = str(request.POST.get('Cfrom',"USD"))
        Cto = str(request.POST.get('Cto',"THB"))
        rates = str(getRates(Cfrom,Cto))
        results = str(convert(amt,Cfrom,Cto))

        Currency_Exchanger.objects.create(amt = amt,
                                          F_text = Cfrom,
                                          S_text = Cto,
                                          rates = rates,
                                          result = results)     
        return redirect("/")

    itemDB = Currency_Exchanger.objects.all()
    return render(request, 'home.html' , {'itemDB': itemDB})

###

def convert(amt,Cfrom,Cto):
    rate = getRates(Cfrom,Cto)
    return float(amt*rate)

def getRates(Cfrom,Cto):
    Cfrom.upper()
    Cto.upper()
    
    USDTHB = 35.50
    THBUSD = 0.031

    if(Cfrom == 'USD' and Cto == 'THB'):
        return USDTHB
    elif(Cfrom == 'THB' and Cto == 'USD'):
        return THBUSD
    return 1

###
