from django.contrib import admin
from Currency_Exchange.models import Currency_Exchanger

class ViewbyAdmin(admin.ModelAdmin):
    fieldsets = [
         ('Detail',{'fields' : ['amt','F_text','S_text','rates']})
    ]
    search_fields = ['F_text','S_text']

admin.site.register(Currency_Exchanger,ViewbyAdmin)
